#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "sensor_msgs/image_encodings.h"
#include "cv_bridge/cv_bridge.h"
#include "geometry_msgs/Twist.h"

class Teleop
{
    public:
        Teleop()
            :it_(nh_),
            win_name("Teleoperation")
    {
        image_sub_ = it_.subscribe("sensor/kinect/image_generator/bgr_image", 1, &Teleop::bgrImageCB, this);
        vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);

        cv::namedWindow(win_name);
    }

        void bgrImageCB(const sensor_msgs::ImageConstPtr &image)
        {
            cv_bridge::CvImagePtr cv_ptr;    
            try
            {
                cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);
            }
            catch(cv_bridge::Exception &e)
            {
                ROS_ERROR("CV_BRIDGE Exception: %s", e.what());
                return;
            }

            cv::flip(cv_ptr->image, cv_ptr->image, 1);

            cv::imshow(win_name, cv_ptr->image);

            char c = cv::waitKey(1);

            if( (c >= 81) && (c <= 84) )
            {
                ROS_INFO("if");
                geometry_msgs::Twist vel_cmd; 
                if( c == 81 ){
                    vel_cmd.linear.x = 0.0;
                    vel_cmd.angular.z = 2.0;
                }else if(c == 82 ){
                    vel_cmd.linear.x = 2.0;
                    vel_cmd.angular.z = 0.0;
                }else if( c == 83 ){
                    vel_cmd.linear.x = 0;
                    vel_cmd.angular.z = -2.0;
                }else if( c == 84 ){
                    vel_cmd.linear.x = -2.0;
                    vel_cmd.angular.z = 0.0;
                }

                vel_pub_.publish(vel_cmd);
            }
            /*
            else if(c == 32)
            {
                geometry_msgs::Twist vel_cmd;

                vel_cmd.linear.x = 0.0;
                vel_cmd.angular.z = 0.0;

                vel_pub_.publish(vel_cmd);
            }
            */
        }
    private:
        ros::NodeHandle nh_;

        image_transport::ImageTransport it_;
        image_transport::Subscriber image_sub_;
        ros::Publisher vel_pub_;
        std::string win_name;
};

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "teleop_node");

    Teleop teleoperation;

    ros::spin();

    return 0;
}
